# Algoritme glouton

                            **Rendu de monnaie**

Un achat dit en espèces se traduit par un échange de pièces et de billets. Dans la suite de cet exposé, les pièces désignent indifféremment les véritables pièces que les billets. Supposons qu’un achat induise un rendu de 49 euros. Quelles pièces peuvent être rendues? La réponse, bien qu’évidente, n’est pas unique. Quatre pièces de 10 euros, 1 pièce de 5 euros et deux pièces de 2 euros conviennent. Mais quarante-neuf pièces de 1 euros conviennent également! Si la question est de rendre la monnaie avec un minimum de pièces, le problème change de nature. Mais la réponse reste simple : c’est la première solution proposée.


Toutefois, comment parvient-on à un tel résultat ? Quels choix ont été faits qui optimisent le nombre de pièces rendus? C’est le problème du rendu de monnaie dont la solution dépend du système de monnaie utilisé.

 Dans le système monétaire français, les pièces prennent les valeurs 1, 2, 5, 10, 20, 50, 100 euros. Pour simplifier, nous nous intéressons seulement aux valeurs entières et oublions l’existence du billet de 500 euros. Rendre 49 euros avec un minimum de pièces est un problème d’optimisation. En pratique, sans s’en rendre compte généralement, tout individu met en œuvre un algorithme glouton. Il choisit d’abord la plus grandeur valeur de monnaie, parmi 1, 2, 5, 10, contenue dans 49 euros. En l’occurrence, quatre fois une pièce de 10 euros. La somme de 9 euros restant à rendre, il choisit une pièce de 5 euros, puis deux pièces de 2 euros. Cette stratégie gagnante pour la somme de 49 euros l’est-elle pour n’importe quelle somme à rendre ? On peut montrer que la réponse est positive pour le système monétaire français. Pour cette raison, un tel système de monnaie est qualifié de canonique.


D’autres systèmes ne sont pas canoniques. L’algorithme glouton ne répond alors pas de manière optimale. Par exemple, avec le système {1, 3, 6, 12, 24, 30}, l’algorithme glouton répond en proposant le rendu 49 = 30 + 12 + 6 + 1, soit 4 pièces alors que la solution optimale est 49 = 2 × 24 + 1, soit 3 pièces. La réponse à cette difficulté passe par la programmation dynamique, thème abordé en spécialité NSI de classe de terminale.
Un algorithme glouton 


Considérons un ensemble de n pièces de monnaie de valeurs :
        v1 < v2 < ⋅ ⋅ ⋅ < vn 
        avec v1 = 1.


 On suppose que ce système est canonique. On peut noter le système de pièces : 
        Sn = {v1, . . . , vn−1} 


Désignons par s une somme à rendre avec le minimum de pièces de Sn. L’algorithme glouton sélectionne la plus grande valeur vn et la compare à s.
    - Si s < vn, la pièce de valeur vn ne peut pas être utilisée. On reprend l’algorithme avec le système de pièces Sn−1.
    -  Si s ⩾ vb, la pièce vn peut être utilisée une première fois. Ce qui fait une première pièce à comptabiliser, de valeur vn, la somme restant à rendre étant alors s − vn. L’algorithme continue avec la même système de pièces Sn et cette nouvelle somme à rendre s − vn.
L’algorithme est ainsi répété jusqu’à obtenir une somme à rendre nulle. Remarque. Il s’agit effectivement d’un algorithme glouton, la plus grande valeur de pièce étant systématiquement choisie si sa valeur est inférieure à la somme à rendre. Ce choix ne garantit en rien l’optimalité globale de la solution. Le choix fait est considéré comme pertinent et permet d’avancer plus avant dans le calcul. Toutefois, comme nous l’écrivions plus haut, si le système monétaire est canonique, la solution est optimale. Pour savoir si le système est canonique.


